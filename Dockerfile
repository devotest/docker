FROM ubuntu:16.04
MAINTAINER andriy.dudych@globaltechmakers.com
 
RUN apt-get -y update && apt-get -y install git &&  apt-get -y install  nginx 

RUN echo 'Hello Yurec Ogurec! :))' > /var/www/html/index.html
 
EXPOSE  80

ADD hello.html /var/www/

ADD default /etc/nginx/sites-available/default


CMD ["nginx", "-g", "daemon off;"]
